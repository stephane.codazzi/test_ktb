## Installation

- git clone git@gitlab.com:stephane.codazzi/test_ktb.git 
- cd test_ktb
- composer install
- php bin/console doctrine:migrations:migrate
